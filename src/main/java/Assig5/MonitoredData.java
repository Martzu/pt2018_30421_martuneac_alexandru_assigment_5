package Assig5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class MonitoredData
{

    private String startTime;
    private String endTime;
    private String activity;
    private long duration;

    public MonitoredData(String startTime, String endTime, String activity)
    {
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

        Date start = null;
        Date end = null;

        try {
            start = format.parse(startTime);
            end = format.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.duration = end.getTime() - start.getTime();

    }

    public MonitoredData(long duration)
    {
        this.duration = duration;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public long getDuration() {
        return duration;
    }

    public long getHourDuration()
    {
        return this.duration/(60*60*1000);
    }

    public long getMinutesDuration()
    {
        return this.duration/(60*1000)%60;
    }

    public long getSecondsDuration()
    {
        return this.duration/1000%60;
    }

    public String toString()
    {
        return this.startTime + " " + this.endTime + " " + this.activity;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }


    /*public void verify()
    {
        ArrayList<String> days = new ArrayList<>();
        startTime.stream().forEach(x -> {
                    days.add(x.substring(0,10));
                    System.out.println(x);
                }
        );

        //endTime.stream().forEach(x -> System.out.println(x));
        //activity.stream().forEach(x-> System.out.println(x));

        System.out.println(days.stream().distinct().count());
    }*/

}
