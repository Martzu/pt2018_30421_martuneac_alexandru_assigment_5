package Assig5;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        String file = "data.txt";
        List<MonitoredData> data = new ArrayList<>();
        HashMap<String,Integer> log = new HashMap<>();
        HashMap<Integer,HashMap<String,Integer>> totalPerDay = new HashMap<>();

        List<String> days = new ArrayList<>();
        try {
            Stream<String> stream = Files.lines(Paths.get(file));
            stream.forEach(x-> {
                String activity = x.substring(42).replaceAll("\t","");
                MonitoredData monitoredData =new MonitoredData(x.substring(0,19),x.substring(21,40),activity);
                if(log.get(activity) == null)
                {
                    log.put(activity,1);
                }
                else
                {
                    log.put(activity,log.get(activity)+ 1);
                }
                days.add(x.substring(0,10));
                data.add(monitoredData);

            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of days: " + days.stream().distinct().count());

        PrintWriter writer1 = new PrintWriter("activity_count.txt");
        log.entrySet().stream().forEach(x -> writer1.println(x.getKey() + " " + x.getValue()));
        writer1.close();



        HashMap<String,Long> wholePeriod = new HashMap<>();

        for(MonitoredData m : data)
        {
            if(wholePeriod.get(m.getActivity()) == null)
            {
                wholePeriod.put(m.getActivity(),m.getDuration());
            }
            else
            {
                long aux = wholePeriod.get(m.getActivity());
                wholePeriod.put(m.getActivity(),m.getDuration() + aux);
            }
        }

        PrintWriter writer2 = new PrintWriter("activity_10H.txt");
        HashMap<String,Long> overTenH = new HashMap<>();
        System.out.println();
        wholePeriod.entrySet().stream().filter(x-> x.getValue()/(60*60*1000) >= 10).forEach(x ->
                {
                    writer2.println(x.getKey() + " " + x.getValue() / (60 * 60 * 1000) + " " + x.getValue() / (60 * 1000) % 60 + " " + x.getValue() / 1000 % 60);
                    overTenH.put(x.getKey(),x.getValue());
                }
        );
        writer2.close();

        HashMap<String,Integer> perDay = new HashMap<>();
        for(MonitoredData m : data)
        {
            m.setActivity(m.getActivity().replaceAll("\t",""));
            if(totalPerDay.get(Integer.parseInt(m.getStartTime().substring(8,10))) == null)
            {
                perDay = new HashMap<>();
                totalPerDay.put(Integer.parseInt(m.getStartTime().substring(8,10)),perDay);
                if(perDay.get(m.getActivity()) == null)
                {
                    perDay.put(m.getActivity(),1);
                }
                else
                {
                    perDay.put(m.getActivity(),perDay.get(m.getActivity()) + 1);
                }
            }
            else
            {
                if(perDay.get(m.getActivity()) == null)
                {
                    perDay.put(m.getActivity(),1);
                }
                else
                {
                    perDay.put(m.getActivity(),perDay.get(m.getActivity()) + 1);
                }
                totalPerDay.put(Integer.parseInt(m.getStartTime().substring(8,10)),perDay);
            }
        }

        /*data.stream().forEach(m ->
        {
            HashMap<String,Integer> perDay = new HashMap<>();
            if(totalPerDay.get(Integer.parseInt(m.getStartTime().substring(8,10))) == null)
            {

                totalPerDay.put(Integer.parseInt(m.getStartTime().substring(8,10)),perDay);
                if(perDay.get(m.getActivity()) == null)
                {
                    perDay.put(m.getActivity(),1);
                }
                else
                {
                    perDay.put(m.getActivity(),perDay.get(m.getActivity()) + 1);
                }
            }
            else
            {
                if(perDay.get(m.getActivity()) == null)
                {
                    perDay.put(m.getActivity(),1);
                }
                else
                {
                    perDay.put(m.getActivity(),perDay.get(m.getActivity()) + 1);
                }
                totalPerDay.put(Integer.parseInt(m.getStartTime().substring(8,10)),perDay);
            }

        });*/


        PrintWriter writer3 = new PrintWriter("daily_activity.txt");
        totalPerDay.entrySet().stream().forEach(x ->
                {
                    writer3.println(x.getKey() +  " " + x.getValue());
                }

        );
        writer3.close();

        HashMap<String,Integer> lessThan5 = new HashMap<>();

        data.stream().filter(x -> x.getMinutesDuration() < 5 && x.getHourDuration() == 0).forEach(x ->
        {
            if(lessThan5.get(x.getActivity()) == null)
            {
                lessThan5.put(x.getActivity(),1);
            }
            else
            {
                lessThan5.put(x.getActivity(),lessThan5.get(x.getActivity()) + 1);
            }

        });

        List<String> special = new ArrayList<>();

        PrintWriter writer4 = new PrintWriter("90percent_less_5_minutes.txt");
        log.entrySet().stream().filter(x -> lessThan5.get(x.getKey()) != null && (lessThan5.get(x.getKey())/x.getValue() >= 0.9)).forEach(x ->
                {
                    special.add(x.getKey());
                    writer4.println(x.getKey());
                }
        );
        writer4.close();
    }
}
